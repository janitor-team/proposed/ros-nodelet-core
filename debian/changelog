ros-nodelet-core (1.10.2-1) unstable; urgency=medium

  * New upstream version 1.10.2
  * Rebase patches
  * Install version.h

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 08 Oct 2021 09:47:03 +0200

ros-nodelet-core (1.10.1-1) unstable; urgency=medium

  * New upstream version 1.10.1
  * Add lintian override for arch:any.
    Multi-Arch interpreter problem as explained in #901696

 -- Timo Röhling <roehling@debian.org>  Sun, 26 Sep 2021 18:43:42 +0200

ros-nodelet-core (1.10.0-5) unstable; urgency=medium

  * Team upload.
  * Upload to unstable
  * Fix Multiarch hinter issues

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 22:43:23 +0200

ros-nodelet-core (1.10.0-4) experimental; urgency=medium

  * Team upload.
  * Move pkg-config and CMake config files to /usr/lib/<triplet>
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Tue, 21 Sep 2021 00:08:26 +0200

ros-nodelet-core (1.10.0-3) unstable; urgency=medium

  * simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 16:48:37 +0100

ros-nodelet-core (1.10.0-2) unstable; urgency=medium

  * Add patch for Boost 1.74
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 15 Dec 2020 09:58:33 +0100

ros-nodelet-core (1.10.0-1) unstable; urgency=medium

  * New upstream version 1.10.0
  * Remove Thomas from Uploaders, thanks for working on this
  * bump policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 20 Jun 2020 08:47:31 +0200

ros-nodelet-core (1.9.16-2) unstable; urgency=medium

  * Drop Python 2 packages (Closes: #938386)
  * Bump policy version (no changes)
  * switch to debhelper-compat and debhelper 12
  * simplify d/watch
  * add Salsa CI
  * add MA hint

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 25 Oct 2019 16:14:43 +0200

ros-nodelet-core (1.9.16-1) unstable; urgency=medium

  * New upstream version 1.9.16
  * Update copyright
  * Rebase patch
  * Bump policy version (no changes)
  * simplify d/rules
  * Fix lintian warning
  * Bump Soname due to ABI change
  * Add Python 3 package

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 03 Nov 2018 13:49:15 +0100

ros-nodelet-core (1.9.8-2) unstable; urgency=medium

  * add Multi-Arch according to hinter
  * Update watch file
  * Update Vcs URLs to salsa.d.o
  * Add R³
  * http -> https
  * Fix roscpp-msg* dependency
  * Bump policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 31 Jul 2018 22:27:42 +0200

ros-nodelet-core (1.9.8-1) unstable; urgency=medium

  * New upstream version 1.9.8

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 17 Nov 2016 18:35:52 +0100

ros-nodelet-core (1.9.7-2) unstable; urgency=medium

  * Reupload because of broken ros-comm

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 04 Nov 2016 22:11:15 +0100

ros-nodelet-core (1.9.7-1) unstable; urgency=medium

  * New upstream version 1.9.7

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 04 Nov 2016 09:51:04 +0100

ros-nodelet-core (1.9.6-1) unstable; urgency=medium

  * Bumped Standards-Version to 3.9.8, no changes needed.
  * Update Vcs URLs
  * Update my email address
  * New upstream version 1.9.6

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 24 Sep 2016 17:58:25 +0200

ros-nodelet-core (1.9.5-2) unstable; urgency=medium

  * rebuild for changes in ros-genlisp

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Wed, 13 Jul 2016 22:27:16 +0200

ros-nodelet-core (1.9.5-1) unstable; urgency=medium

  * Imported Upstream version 1.9.5

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 10 Jul 2016 23:16:43 +0200

ros-nodelet-core (1.9.3-3) unstable; urgency=medium

  * Adopt to new libexec location in catkin

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Mon, 30 May 2016 13:37:12 +0200

ros-nodelet-core (1.9.3-2) unstable; urgency=medium

  * add dependencies

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 10 Jan 2016 15:13:32 +0100

ros-nodelet-core (1.9.3-1) unstable; urgency=medium

  * Initial release (Closes: #806281)

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sat, 26 Dec 2015 01:34:44 +0000
